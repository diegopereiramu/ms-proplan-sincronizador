import { query } from './pg.connect';

const registrar = async (documento, contrato) => {
    try {
        const q = `
        INSERT INTO "proplan"."contrato" 
            (profesional_id, 
            ley_id, 
            codigo, 
            especialidad_sis,
            fecha_inicio,
            fecha_termino) 
        VALUES (
            (SELECT id FROM "proplan"."profesional" WHERE documento = '${documento.numero}' AND tipo_documento_id = (SELECT id FROM "proplan"."tipo_documento" WHERE codigo = '${documento.tipo}')), 
            (SELECT id FROM "proplan"."ley" WHERE codigo = '${contrato.ley}'), 
            '${contrato.codigo}',
            ${contrato.especialidad_sis != null ? '\''+contrato.especialidad_sis+'\'' : null},
            '${contrato.inicio}',
            ${contrato.termino != null ? '\''+contrato.termino+'\'' : null}
        ) RETURNING id;`
        //console.log(q);
        //process.exit();
        const result = await query(q);

        return result.rows[0].id;
    } catch (error) {
        if(error.code === 'ER_DUP_ENTRY'){
            console.log(`KEY DUPLICADA CONTRATO PROFESIONAL: ${documento.numero}, CONTRATO CODIGO: ${contrato.codigo}, LEY ${contrato.ley}`);

            return null;
        }
        throw error;
    }
};

export { registrar };
