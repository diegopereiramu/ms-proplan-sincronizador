import { query } from './pg.connect';

const registrar = async (profesional, titulo) => {
    try {
        const q = `
        INSERT INTO "proplan"."profesional_titulo" 
            (profesional_id, 
            titulo_id) 
        VALUES (
            (SELECT id FROM "proplan"."profesional" WHERE documento = '${profesional.documento.numero}' AND tipo_documento_id = (SELECT id from "proplan"."tipo_documento" WHERE codigo = '${profesional.documento.tipo}')), 
            (SELECT id FROM "proplan"."titulo" WHERE descripcion = '${titulo}')
        )  RETURNING id;`;
        const result = await query(q);

        return result.rows[0].id;
    } catch (error) {
        throw error;
    }
};

export { registrar };
