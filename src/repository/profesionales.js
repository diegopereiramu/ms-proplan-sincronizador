import { query } from './pg.connect';

const registrar = async (profesional) => {
    console.log(profesional.documento)
    try {
        const result = await query(`
        INSERT INTO "proplan"."profesional" (documento, nombre, tipo_documento_id) VALUES
        ('${profesional.documento.numero}', '${profesional.nombre}', (SELECT id FROM "proplan"."tipo_documento" WHERE codigo = '${profesional.documento.tipo}')) 
           ON CONFLICT (documento) do update set documento = '${profesional.documento.numero}' returning id;
        `);

        return result.rows[0].id;

    } catch (error) {

        throw error;
    }
};

export { registrar };
