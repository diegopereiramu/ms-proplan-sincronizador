import { query } from './pg.connect';

const registrar = async (documento, contrato, centroCosto) => {
    console.log(documento, contrato, centroCosto)
    try {
        const q = `
        INSERT INTO "proplan"."contrato_centro_costo" 
            (contrato_id, 
            centro_costo_id) 
        VALUES (
            (SELECT id FROM "proplan"."contrato" WHERE codigo = '${contrato.codigo}' AND profesional_id = (SELECT id FROM "proplan"."profesional" WHERE documento = '${documento.numero}') AND ley_id = (SELECT id FROM "proplan"."ley" WHERE codigo = '${contrato.ley}')), 
            (SELECT id FROM "proplan"."centro_costo" WHERE codigo = '${centroCosto}')
        )  RETURNING id;`
        // console.log(q);
        // process.exit();
        const result = await query(q);

        return result.rows[0].id;
    } catch (error) {
        if(error.code === 'ER_DUP_ENTRY'){
            console.log(`KEY DUPLICADA CONTRATO CENTRO COSTO: ${documento.numero}, CONTRATO CODIGO: ${contrato.codigo}, LEY ${contrato.ley}`);

            return null;
        }
        throw error;
    }
};

export { registrar };
