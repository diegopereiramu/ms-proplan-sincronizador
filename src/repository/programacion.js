import { query } from './pg.connect';

const exportarOriginal = async () => {
    let export_data = [];
        const q = `select p.documento as rut, p.nombre, l.codigo as ley, c.codigo as correlativo, t.descripcion as titulo, 
       c.especialidad_sis, ce.horas_semanales, ce.sistema_turno, ce.dias_feriados_legales, ce.dias_descanso_compensatorio,
       ce.dias_permiso_administrativo, ce.dias_congreso_capacitacion, ce.tiempo_lactancia_semanal, ce.tiempo_colacion_semanal, 
       ce.fecha_inicio, ce.fecha_termino, centro_costo_id
from "proplan"."programacion"
inner join "proplan"."contrato_extension" ce on programacion.contrato_extension_id = ce.id
inner join "proplan"."contrato" c on ce.contrato_id = c.id
inner join "proplan"."profesional" p on c.profesional_id = p.id
inner join "proplan"."ley" l on c.ley_id = l.id
inner join "proplan"."profesional_titulo" pt on p.id = pt.profesional_id
inner join "proplan"."titulo" t on pt.titulo_id = t.id;`;
    try {
        export_data = await query(q, []);
    } catch (error) {
        console.log(error);
    }

    return export_data.rows;
};


const exportar = async () => {
    let export_data = [];
    const q = `select p.documento as rut, p.nombre, l.codigo as ley, c.codigo as correlativo, t.descripcion as titulo,
    c.especialidad_sis, ce.horas_semanales, ce.sistema_turno, ce.dias_feriados_legales, ce.dias_descanso_compensatorio,
    ce.dias_permiso_administrativo, ce.dias_congreso_capacitacion, ce.tiempo_lactancia_semanal, ce.tiempo_colacion_semanal,
    ce.fecha_inicio, ce.fecha_termino, centro_costo_id
from "proplan"."profesional" p
inner join "proplan"."contrato" c on p.id = c.profesional_id
inner join "proplan"."contrato_extension" ce on ce.contrato_id = c.id
inner join "proplan"."contrato_centro_costo" ccc on c.id = ccc.contrato_id
inner join "proplan"."ley" l on c.ley_id = l.id
inner join "proplan"."profesional_titulo" pt on p.id = pt.profesional_id
inner join "proplan"."titulo" t on pt.titulo_id = t.id;`;
    try {
        export_data = await query(q, []);
    } catch (error) {
        console.log(error);
    }

    return export_data.rows;
};
export { exportar };
