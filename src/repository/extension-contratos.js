import { query } from './pg.connect';
const moment = require('moment');
const registrar = async (documento, contrato, fechaInicio, fechaFin) => {
    try {
        const q = `
        INSERT INTO "proplan"."contrato_extension" 
            (contrato_id,
            horas_semanales, 
            dias_feriados_legales, 
            dias_descanso_compensatorio, 
            dias_permiso_administrativo, 
            dias_congreso_capacitacion, 
            tiempo_lactancia_semanal, 
            tiempo_colacion_semanal, 
            sistema_turno, 
            fecha_inicio,
            fecha_termino) 
        VALUES (
            (Select id from "proplan"."contrato" where codigo = ${contrato.codigo}),
            ${contrato.horas_semanales},
            ${contrato.dias.feriados_legales},
            ${contrato.dias.descanso_compensatorio},
            ${contrato.dias.permiso_administrativo},
            ${contrato.dias.congreso_capacitacion},
            ${contrato.tiempo.lactancia_semanal},
            ${contrato.tiempo.colacion_semanal},
            ${contrato.sistema_turno},
            '${fechaInicio||contrato.inicio}',
            '${fechaFin||contrato.termino}'
        ) RETURNING id;`
        const result = await query(q);
        const updateDate = await updateExtension(documento, contrato, fechaInicio, fechaFin, result.rows[0].id);
        return result.rows[0].id;
    } catch (error) {

        throw error;
    }
};

const updateExtension = async (documento, contrato, fechaInicio, fechaFin, newId) => {
    try {
        const q = `UPDATE "proplan"."contrato_extension" SET activo = false, fecha_termino = $1
        where id IN (
        select * from
          (
            select id
            from "proplan"."contrato_extension"
            WHERE contrato_id = (Select id from "proplan"."contrato" where codigo = ${contrato.codigo}) AND id != ${newId} AND activo = true
          ) as id);`
          const result = await query(q, [
            moment(fechaInicio).subtract(1,'d').format('YYYY-MM-DD')
        ]);
        console.log(result);
        // return result.rows[0].id;
    } catch (error) {
        throw error;
    }
};

export { registrar };
