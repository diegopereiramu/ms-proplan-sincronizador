const { getJsDateFromExcel } = require("excel-date-to-js");
const moment = require('moment');

const hello = () => {
    return 'hello';
};

const mapToContrato = (row) => {
    try{
        return {
            ley: row[3].toString().replace('LEY', '').replace('.','').replace(' ',''),
            codigo: row[4],
            especialidad_sis: row[6] === '' || row[6] == '0' || row[6] == null || row[6] == undefined ? null : row[6],
            horas_semanales: row[7],
            sistema_turno: row[8] == 'S' ? true : false,                 
            dias:{
                feriados_legales: row[10] === undefined ? 0 : row[10],
                descanso_compensatorio: row[11] === undefined ? 0 : row[11],
                permiso_administrativo: row[12] === undefined ? 0 : row[12],
                congreso_capacitacion: row[13] === undefined ? 0 : row[13],
            },
            tiempo:{
                lactancia_semanal: row[14] === undefined ? 0 : row[14],
                colacion_semanal: row[15] === undefined ? 0 : row[15]
            },
            inicio: typeof row[16] == 'number' ? moment(getJsDateFromExcel(row[16])).format('YYYY/MM/DD') : moment(row[16]),
            termino: row[17] === '' || row[17] === null || row[17] === undefined ? null : typeof row[17] == 'number' ? moment(getJsDateFromExcel(row[17])).format('YYYY/MM/DD') : moment(row[17]),
            centro_costos: row[18] !== '' ? [row[18].toString().split(',')] : []
        };
    }catch(e){
        console.log('ERROR en FILA',row)
        console.log(e);
    }
};

const mapToProfesionales = (rows) => {
    let profesionales = [];
    let contrato = {};
    let iProfecionalExistente = 0;
    let profesional = {};

    rows.data.forEach((row, index) => {        
        if(index > 0 && row[0] !== undefined){
            profesional =  {
                nombre: row[2],
                documento:{
                    numero: row[0].toString()+row[1].toString(),
                    tipo: 'RUT'
                },
                titulos: [row[5]],
                contratos: []
            };
            contrato = mapToContrato(row);
            iProfecionalExistente = profesionales.findIndex(p => p.documento.numero == profesional.documento.numero && p.documento.tipo == profesional.documento.tipo);
            if(iProfecionalExistente < 0){
                profesional.contratos.push(contrato);
                profesionales.push(profesional);
            }else{
                profesionales[iProfecionalExistente].contratos.push(contrato);
            }
        }
    });
    return profesionales;
};

const mapExcelData = (data) => {
    const newArray = [["RUT Programable","DV","Nombre","LEY","Correlativo Contrato","Titulo Profesional","Especialidad SIS","Hrs Semanales contratadas",
        "Sistema de Turno (S/N)"	,"Observaciones debe Identificar (Liberado de Guardia (LG)/Periodo Asistencial Obligatorio(PAO))","Feriados Legales",
        "Dias Descanso Compensatorio (Ley Urgencia)", "Dias de Permisos Administrativos","Dias de Congreso o capacitación",	"Tiempo de Lactancia semanal (min)",
        "Tiempo de colación semanal (min)",	"Fecha Inicio contrato (AAAAMMDD)", "Fecha termino contrato (AAAAMMDD)", "Centro de costos"]];
    data.forEach(t => {
        t.dv = t.rut.slice(t.rut.length - 1);
        if(t.sistema_turno)
            t.sistema_turno="S";
        else
            t.sistema_turno="N";
      /*const anoInicio= t.fecha_inicio.toISOString().replace('T', ' ').substr(0, 4);
      const mesInicio= t.fecha_inicio.toISOString().replace('T', ' ').substr(5, 2);;
      const diaInicio= t.fecha_inicio.toISOString().replace('T', ' ').substr(8, 2);
      const inicio=diaInicio+"/"+mesInicio+"/"+anoInicio;
      const anoFinal= t.fecha_termino.toISOString().replace('T', ' ').substr(0, 4);
      const mesFinal= t.fecha_termino.toISOString().replace('T', ' ').substr(5, 2);;
      const diaFinal= t.fecha_termino.toISOString().replace('T', ' ').substr(8, 2);
      const termino=diaFinal+"/"+mesFinal+"/"+anoFinal;
        console.log(inicio);*/
        var finicio = 25569.0 + ((t.fecha_inicio.getTime() - (t.fecha_inicio.getTimezoneOffset() * 60 * 1000)) / (1000 * 60 * 60 * 24));
        var ftermino  = 25569.0 + ((t.fecha_termino.getTime() - (t.fecha_termino.getTimezoneOffset() * 60 * 1000)) / (1000 * 60 * 60 * 24));
        t.rut = t.rut.slice(0, -1).replace(/\D/g,'');
        newArray.push([t.rut, t.dv, t.nombre, "LEY "+t.ley, t.correlativo, t.titulo, t.especialidad_sis, t.horas_semanales, t.sistema_turno, "", t.dias_feriados_legales, t.dias_descanso_compensatorio, t.dias_permiso_administrativo, t.dias_congreso_capacitacion, t.tiempo_lactancia_semanal, t.tiempo_colacion_semanal, finicio, ftermino, t.centro_costo_id])

    })
    return newArray;
};

export {
    hello,
    mapToProfesionales,
    mapExcelData
};
