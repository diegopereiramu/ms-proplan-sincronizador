import { sincronizar } from '@actions/rrhh';
import { sincronizarUnProfesional } from '@actions/rrhh';
import { exportarProgramacion } from '@actions/rrhh';


export default (router, app) => {
    router.post('/rrhh', sincronizar);
    router.post('/rrhh-save', sincronizarUnProfesional);
    router.post('/excel', exportarProgramacion);
};