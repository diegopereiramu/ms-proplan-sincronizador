import api from '@amacl/roboc-api';
import { apiOptions } from '@config';
import rrhh from '@routes/rrhh';
const upload = require("express-fileupload");

const run = () => {
    api(apiOptions,(router, app) => {
        app.use(upload());
        rrhh(router);
        return router;
    });
};

export {
    run,
}