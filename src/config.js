const apiOptions = {
    prefix: process.env.APP_PREFIX,        
    port: process.env.APP_PORT,
    host: process.env.APP_HOST,
    alive: [process.env.APP_HEALTH_PATH, (req, res) => {
        res.status(200).json({
            OK:true,
            status:200,
            message:"health",
        }); 
    }],
};

export {
    apiOptions,
};