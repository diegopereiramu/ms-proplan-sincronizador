import { registrar as registrarProfesional } from  '@repository/profesionales';
import { registrar as registrarContrato } from  '@repository/contratos';
import { registrar as registrarCentroCosto } from  '@repository/centrocostos';
import { registrar as registrarExtensionContrato } from  '@repository/extension-contratos';
import { registrar as registrarTitulo } from  '@repository/titulos';
import { exportar as exportar } from '@repository/programacion';
import xlsx from 'node-xlsx';
import { mapToProfesionales } from '@helpers/utils';
import { mapExcelData } from '@helpers/utils';
const moment = require('moment');

const sincronizar = async (req, res, next) => {
    try {
        const planilla = xlsx.parse(req.files.excel.data);
        const fechaInicio = req.body.inicio;
        const fechaFin = req.body.fin;
        //console.log(planilla[0]);
        //process.exit();
        //INICIO VALIDACION

        //FIN VALIDACION
        const profesionales = mapToProfesionales(planilla[0]);
        console.log(`cantidad profesionales: ${profesionales.length}`);
        const promises = [];
        profesionales.forEach(profesional => {
            promises.push(new Promise(async (resolve, reject) => {
                try {
                    await registrarProfesional(profesional);    
                    profesional.contratos.forEach(async contrato => {
                        console.log('---------------------- Genera el contrato ----------------------');
                        await registrarContrato(profesional.documento, contrato);
                        console.log('---------------------- Genera la extensión del contrato ----------------------');
                        await registrarExtensionContrato(profesional.documento, contrato, fechaInicio, fechaFin);
                        console.log('---------------------- Asociasion de centros de costos ----------------------');
                        contrato.centro_costos.forEach(async centroCosto => {
                            await registrarCentroCosto(profesional.documento, contrato, centroCosto);
                        });                    
                    });
                    profesional.titulos.forEach(async titulo =>{
                        await registrarTitulo(profesional, titulo);
                    });
                    resolve(true);
                } catch (error) {
                    console.log(error);   
                    resolve(true);
                }
            }));
        });
        const results = await Promise.all(promises);
        res.status(200).json({
            OK:true
        });   
    } catch (error) {
        res.status(200).json({
            OK:false,
            error: error.message
        }); 
    }
       
};

const sincronizarUnProfesional = async (req, res, next) => {
    try {
        console.log(req.body)
        let profesionales = [];
        // const planilla = xlsx.parse(req.files.excel.data);
        const fechaInicio =  moment(req.body.fechaInicioProgramacion).format('YYYY/MM/DD');
        const fechaFin = moment(req.body.fechaFinProgramacion).format('YYYY/MM/DD');
        console.log(fechaInicio, fechaFin)
        profesionales.push(req.body);
        // console.log(`cantidad profesionales: ${profesionales.length}`);
        const promises = [];
        profesionales.forEach(profesional => {
            promises.push(new Promise(async (resolve, reject) => {
                try {
                    console.log('---------------------- Genera el contrato-1 ----------------------');
                    await registrarProfesional(profesional);   
                        console.log('---------------------- Genera el contrato ----------------------');
                        await registrarContrato(profesional.documento, profesional.contratos);
                        console.log('---------------------- Genera la extensión del contrato ----------------------');
                        await registrarExtensionContrato(profesional.documento, profesional.contratos, fechaInicio, fechaFin);
                        console.log('---------------------- Asociasion de centros de costos ----------------------');

                            // console.log(profesional.contratos)
                            await registrarCentroCosto(profesional.documento, profesional.contratos, profesional.contratos.centro_costos);

                    // profesional.titulos.forEach(async titulo =>{
                        await registrarTitulo(profesional, profesional.titulos);
                    // });
                    resolve(true);
                } catch (error) {
                    console.log(error);   
                    resolve(true);
                }
            }));
        });
        const results = await Promise.all(promises);
        res.status(200).json({
            OK:true
        });   
    } catch (error) {
        res.status(200).json({
            OK:false,
            error: error.message
        }); 
    }
       
};

const extensionDeContrato = async (req, res, next) => {
    try {
        console.log(req.body)
        let profesionales = [];
        // const planilla = xlsx.parse(req.files.excel.data);
        const fechaInicio =  moment(req.body.fechaInicioProgramacion).format('YYYY/MM/DD');
        const fechaFin = moment(req.body.fechaFinProgramacion).format('YYYY/MM/DD');
        console.log(fechaInicio, fechaFin)
        profesionales.push(req.body);
        // console.log(`cantidad profesionales: ${profesionales.length}`);
        const promises = [];
        profesionales.forEach(profesional => {
            promises.push(new Promise(async (resolve, reject) => {
                try {
                    console.log('---------------------- Genera el contrato-1 ----------------------');
                    await registrarProfesional(profesional);   
                        console.log('---------------------- Genera el contrato ----------------------');
                        await registrarContrato(profesional.documento, profesional.contratos);
                        console.log('---------------------- Genera la extensión del contrato ----------------------');
                        await registrarExtensionContrato(profesional.documento, profesional.contratos, fechaInicio, fechaFin);
                        console.log('---------------------- Asociasion de centros de costos ----------------------');
                        profesional.contratos.centro_costos.forEach(async centroCosto => {
                            // console.log(profesional.contratos)
                            await registrarCentroCosto(profesional.documento, profesional.contratos, centroCosto);
                        });   
                    // profesional.titulos.forEach(async titulo =>{
                        await registrarTitulo(profesional, profesional.titulos);
                    // });
                    resolve(true);
                } catch (error) {
                    console.log(error);   
                    resolve(true);
                }
            }));
        });
        const results = await Promise.all(promises);
        res.status(200).json({
            OK:true
        });   
    } catch (error) {
        res.status(200).json({
            OK:false,
            error: error.message
        }); 
    }
       
};

const exportarProgramacion = async(req,res, next) => {
    let programacion = [];
    try {
        programacion = await exportar();
        programacion = mapExcelData(programacion)
        res.status(200).json({
            OK:true,
            programacion: programacion,
        });
    } catch (error) {
        res.status(200).json({
            OK:false,
            programacion: programacion,
        });
    }
}

export {
    sincronizar,
    sincronizarUnProfesional,
    exportarProgramacion
};
