# Microservicio Sincronizador

## Clonar

```sh
$ git clone https://bitbucket.org/amarischile/ms-proplan-sincronizador.git
$ cd ms-proplan-sincronizador
$ cp .env .env.local
$ npm install
$ npm test
$ npm run start:local
```
> Debes configurar las variables de entorno para tu desarrollo local en .env.local

## Docker

```sh
$ docker build -t ms-proplan-sincronizador .

$ docker run -it \
-e APP_PORT=1000 \
-e APP_HOST=0.0.0.0 \
-e APP_PREFIX=/ \
-e APP_HEALTH_PATH=/health \
-e MYSQL_CONNECTION_STRING="mysql://user:pass@server/dbname?debug=true&charset=UTF8_GENERAL_CI" \
-p 1000:1000 --name ms-proplan-sincronizador ms-proplan-sincronizador
```

## Verificar

Verificar que el servicio este arriba

```sh
$ curl -v http://localhost:1000/health
```